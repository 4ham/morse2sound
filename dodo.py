import os
import os.path
from typing import Any, Generator

import doit.tools  # type: ignore

SUPPORTED_PYTHON_VERSIONS = ["3.12", "3.11", "3.10"]


def task_grab_base_image() -> dict[str, Any]:
    """Grab the base Debian docker image."""
    return {
        "actions": ["docker pull debian:bookworm-slim"],
        "uptodate": [doit.tools.run_once],
    }


def task_grab_python_image() -> Generator[dict[str, Any], None, None]:
    """Grab the docker images for various Python versions."""
    for py_ver in SUPPORTED_PYTHON_VERSIONS:
        yield {
            "name": py_ver,
            "actions": [f"docker pull python:{py_ver}-slim-bookworm"],
            "task_dep": ["grab_base_image"],
            "uptodate": [doit.tools.run_once],
        }


def task_setup_venv() -> Generator[dict[str, Any], None, None]:
    """Set up a venv usable from the docker container."""
    pwd = os.path.dirname(os.path.abspath(__file__))
    uid = os.getuid()

    def mk_cache_dir(cache_dir: str) -> None:
        os.makedirs(cache_dir, mode=0o755, exist_ok=True)

    for py_ver in SUPPORTED_PYTHON_VERSIONS:

        yield {
            "name": py_ver,
            "actions": [
                (mk_cache_dir, [".cache"]),
                f"docker run --rm -i --pull=never "
                f"-v {pwd}:/morse2sound "
                f"-v {pwd}/.cache:/.cache "
                f"-w /morse2sound -u {uid} python:{py_ver}-slim-bookworm "
                "/bin/bash -c "
                f"'rm -rf venv-{py_ver} && "
                f"python -m venv venv-{py_ver} && "
                f". venv-{py_ver}/bin/activate && "
                f"pip install -r requirements_test.frozen'",
            ],
            "file_dep": ["requirements_test.frozen"],
            "uptodate": [doit.tools.run_once],
            "task_dep": [f"grab_python_image:{py_ver}"],
            "targets": [f"venv-{py_ver}"],
        }


def task_run_tests() -> Generator[dict[str, Any], None, None]:
    """Run the test for python version."""
    pwd = os.path.dirname(os.path.abspath(__file__))
    uid = os.getuid()
    for py_ver in SUPPORTED_PYTHON_VERSIONS:
        yield {
            "name": py_ver,
            "actions": [
                f"docker run --rm -i --pull=never "
                f" -v {pwd}:/morse2sound -w /morse2sound -u {uid} python:{py_ver}-slim-bookworm /bin/bash -c "
                f"'. venv-{py_ver}/bin/activate && ./lint.py --check'"
            ],
            "task_dep": [f"setup_venv:{py_ver}"],
        }
