#!/usr/bin/env python

import os
import subprocess
from argparse import ArgumentParser

FILES = ["src", "tests", "lint.py", "dodo.py"]


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        description="Run some automatic code prettifying and linting on the sources."
    )
    arg_parser.add_argument(
        "--check",
        action="store_true",
        help="Do not try to repair, just check all is well.",
    )
    args = arg_parser.parse_args()

    project_root_dir = os.path.dirname(os.path.dirname(__file__))
    src_dir = os.path.join(project_root_dir, "src")
    if "PYTHONPATH" in os.environ:
        pythonpath = f"{src_dir}{os.pathsep}{os.environ['PYTHONPATH']}"
    else:
        pythonpath = src_dir

    env = dict(os.environ)
    env["PYTHONPATH"] = pythonpath

    if args.check:
        subprocess.run(["isort", "--check", *FILES], check=True, env=env)
        subprocess.run(["black", "--check", *FILES], check=True, env=env)
    else:
        subprocess.run(["isort", *FILES], check=True, env=env)
        subprocess.run(["black", *FILES], check=True, env=env)

    subprocess.run(
        ["flake8", "--max-line-length=120", "--color=never", *FILES],
        check=True,
        env=env,
    )

    subprocess.run(["mypy", "--strict", *FILES], check=True, env=env)

    subprocess.run(
        ["py.test", "--cov-report", "term-missing", "--cov", "cw_enc", "-v"], env=env
    )
